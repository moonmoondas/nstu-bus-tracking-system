# NSTU Bus Tracking System
An android based bus tracking system that provide the real-time location and direction of the bus and show 
it to the user who wants to travel by the bus. The driver will share his location that’s will show to 
the user the current position of bus. The system will reduce the waiting time for travelling.

[N.B.: Due to commercial purpose I don't share the full code here. You can just get the idea from here.] 

## Features
1. Real time bus tracking in single map
2. Firebase as backend
3. Simple coding style
4. Beautiful UI/UX with material design 

