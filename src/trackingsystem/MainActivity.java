package com.moon.trackingsystem;



import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


/**
 * This is the initial activity.
 * This activity start when the app start.
 *
 * When the will start first time on a device this activity redirect the users to
 * different activity based on their state.
 *
 * Also It has a splash window. Which will disappear after 1.5 seconds.
 */
public class MainActivity extends AppCompatActivity {

    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.main_icon);
        Animation animation= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_clockwise);
        imageView.startAnimation(animation);

        ObjectAnimator scaleDown = ObjectAnimator.ofPropertyValuesHolder(
                imageView,
                PropertyValuesHolder.ofFloat("scaleX", 1.2f),
                PropertyValuesHolder.ofFloat("scaleY", 1.2f));
        scaleDown.setDuration(310);

        scaleDown.setRepeatCount(ObjectAnimator.INFINITE);
        scaleDown.setRepeatMode(ObjectAnimator.REVERSE);

        scaleDown.start();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(this.getResources().getColor(R.color.white, null));
        }
        new Handler().postDelayed(() -> {

            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) {

                FirebaseAuth.getInstance().getCurrentUser().reload();

                if(user.isEmailVerified()){
                    Log.e("TEST","here");
                    Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Log.e("TEST","here1");
                    Intent intent = new Intent(MainActivity.this, VerifyActivity.class);
                    startActivity(intent);
                    finish();
                }

            } else {
                Intent intent = new Intent(MainActivity.this, AuthenticationActivity.class);
                startActivity(intent);
                finish();
            }
        }, 1500);

    }
}
