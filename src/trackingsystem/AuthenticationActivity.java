package com.moon.trackingsystem;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Activity for Course Authentication.
 *
 */
public class AuthenticationActivity extends AppCompatActivity {

    private EditText email, password;
    private Button login;
    private TextView register;
    private FirebaseAuth auth;

    private ImageView icon1,icon2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        login = findViewById(R.id.loginbutton);
        register = findViewById(R.id.registerbutton);
        auth = FirebaseAuth.getInstance();

        icon1=findViewById(R.id.image1);
        icon2=findViewById(R.id.image2);

        Animation animation= AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_clockwise);
        icon1.startAnimation(animation);
        icon2.startAnimation(animation);


        login.setOnClickListener(v -> {
            String txt_email = email.getText().toString();
            String txt_password = password.getText().toString();

            if (TextUtils.isEmpty(txt_email) || TextUtils.isEmpty(txt_password)) {
                Toast.makeText(AuthenticationActivity.this, "Empty Credentials", Toast.LENGTH_SHORT).show();
            } else {
                auth.signInWithEmailAndPassword(txt_email, txt_password).addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Toast.makeText(AuthenticationActivity.this, "Login successful!", Toast.LENGTH_SHORT).show();

                        if(auth.getCurrentUser().isEmailVerified()){
                            startActivity(new Intent(AuthenticationActivity.this, HomeActivity.class));
                            finish();
                        }else{
                            startActivity(new Intent(AuthenticationActivity.this, VerifyActivity.class));
                            finish();
                        }

                    } else {
                        Toast.makeText(AuthenticationActivity.this, "Wrong email & password!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


        register.setOnClickListener(v -> {
            startActivity(new Intent(AuthenticationActivity.this, RegisterUserActivity.class));
            finish();
        });
    }
}
